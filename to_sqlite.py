from blocklist import parse_master, get_domains_from_blocklist
from main import logger
import tqdm
import sys
import sqlite3

if __name__ == '__main__':
	blocklists = parse_master()
	db = sqlite3.connect('blocklist-ticked.db')
	db.executescript('PRAGMA journal_mode=WAL;')

	with db:
		db.executescript("""
		BEGIN TRANSACTION;
			CREATE TABLE IF NOT EXISTS `domains` (
				`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
				`hostname`	TEXT NOT NULL UNIQUE,
				`blocklist`	INTEGER
			);
			CREATE TABLE IF NOT EXISTS `blocklists` (
				`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
				`url`	TEXT NOT NULL UNIQUE
			);
			CREATE UNIQUE INDEX IF NOT EXISTS `uniq_domains` ON `domains` (`hostname` );
			CREATE INDEX IF NOT EXISTS `blocklist_urls_asc` ON `blocklists` (`url` ASC);
			COMMIT;
		""")

	for blocklist in tqdm.tqdm(blocklists):

		with db:
			db.execute('INSERT OR IGNORE INTO blocklists (url) VALUES (?)', (str(blocklist),))
		blid = -1
		with db:
			c = db.cursor()
			c.execute('SELECT id FROM blocklists WHERE url = ?', (str(blocklist),))
			blid = c.fetchone()[0]

		domains = []
		try:
			domains = get_domains_from_blocklist(blocklist)
		except Exception as ex:
			logger.error('Unable to fetch blocklist: %s because %s' % (blocklist, str(ex)))
			continue
		
		
		hosts = []
		for d in domains:
			d = d.lower()
			hosts.append((blid, d))

		with db:
			db.executemany('INSERT OR IGNORE INTO domains (blocklist, hostname) VALUES (?, ?)', hosts)
			
		
		logger.info('Got %d hostnames from %s' % (len(hosts), blocklist[-35:]))

	with db:
		db.executescript('VACUUM;')
	db.close()