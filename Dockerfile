FROM pypy:3

WORKDIR /usr/src/app

RUN git clone https://gitlab.com/Sharky/blocklist2bind.git .
RUN pypy3 -m pip install -r requirements.txt

# ToDo: make this happen periodically
RUN	pypy3 to_sqlite.py
	
ENTRYPOINT ["pypy3", "twisted-dnsblocker.py"]