#! /home/sharky/.pypy-local/current/bin/pypy3

import argparse
import sqlite3
from main import logger

DEBUG = False
db = None


argparser = argparse.ArgumentParser(description="transparent DNS proxy using a sqlite3 blocklist")
argparser.add_argument("-p", "--port", type=int, default=5353, help="port to listen on (default: 5353)")
argparser.add_argument("-d", "--debug", action='store_true', help='debug mode (so far it only disables moving the database to memory)')
argparser.add_argument("-r", "--resolver", type=str, default="1.1.1.1", help="upstream resolver IP (default 1.1.1.1:53)")

import socket

from twisted.internet.protocol import Factory, Protocol
from twisted.internet import reactor
from twisted.names import dns
from twisted.names import client, server

class FilteredDNSServerFactory(server.DNSServerFactory):
	"""
	def gotResolverResponse(self, *args, **kwargs):
		#(ans, auth, add), protocol, message, address

		c = db.cursor()

		message = args[2]
		logger.debug(message)

		for q in message.queries:
			queryname = str(q.name)
			c.execute('SELECT count(id) FROM domains WHERE hostname LIKE ?', (queryname,))
			row = c.fetchone()
			if row is not None:
				num = row[0]
				if num > 0:
					logger.debug('Blocked hostname found: %s' % (queryname,))
		
		c.close()
	
		super(FilteredDNSServerFactory, self).gotResolverResponse(*args, **kwargs)
	"""

	
	def handleQuery(self, message, protocol, address):
		c = db.cursor()

		for q in message.queries:
			queryname = str(q.name)
			c.execute('SELECT count(id) FROM domains WHERE hostname = ?', (queryname,))
			row = c.fetchone()
			if row is not None:
				num = row[0]
				if num > 0:
					c.close()
					logger.debug('Blocked hostname: %s' % (queryname,))

					response = dns._responseFromMessage(
						responseConstructor=self._messageFactory,
						message=message,
						recAv=self.canRecurse,
						rCode=dns.EREFUSED,
						auth=False
					)

					### Taken straight from: https://github.com/twisted/twisted/blob/twisted-18.4.0/src/twisted/names/server.py
					# XXX: Timereceived is a hack which probably shouldn't be tacked onto
					# the message. Use getattr here so that we don't have to set the
					# timereceived on every message in the tests. See #6957.
					response.timeReceived = getattr(message, 'timeReceived', None)

					# XXX: This is another hack. dns.Message.decode sets maxSize=0 which
					# means that responses are never truncated. I'll maintain that behaviour
					# here until #6949 is resolved.
					response.maxSize = message.maxSize

					response.answers = []
					response.authority = []
					response.additional = []

					self.sendReply(protocol, response, address)
					return


		
		c.close()
		#logger.debug('%s' % (message,))
		return super(FilteredDNSServerFactory, self).handleQuery(message, protocol, address)
	

def main():
	global db
	args = argparser.parse_args()

	if args.debug is False:
		# make a in-memory copy of the database at boot
		db = sqlite3.connect(':memory:')
		logger.info('Making an in-memory copy of the database for speed ...')
		with sqlite3.connect('blocklist-ticked.db') as src:
			for line in src.iterdump():
				db.executescript(line)
	else:
		db = sqlite3.connect('blocklist-ticked.db')

	ipport = args.resolver
	if ':' in ipport:
		p = ipport.split(':')
		ipport = (p[0], int(p[1]))
	else:
		ipport = (ipport, 53)

	logger.info('Using upstream resolver: %s' % (ipport,))

	resolver = client.Resolver(servers=[ipport])
	factory = FilteredDNSServerFactory(clients=[resolver])
	protocol = dns.DNSDatagramProtocol(factory)

	logger.info('Now listening on port %d udp+tcp' % (int(args.port),))
	reactor.listenUDP(int(args.port), protocol)
	reactor.listenTCP(int(args.port), factory)
	reactor.run()

if __name__ == '__main__':
	main()
