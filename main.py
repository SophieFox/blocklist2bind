import sys, os, re, tqdm, logging
import colorlog
from requests import get as http_get

EMPTY_ZONE = r'/etc/bind/db.empty'
ZONE_DEF = r'zone "%s" { type master; notify no; file "%s"; };'


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class TqdmHandler(logging.StreamHandler):
	def __init__(self):
		logging.StreamHandler.__init__(self)
		self.setFormatter(colorlog.ColoredFormatter(
			'%(log_color)s%(name)s | %(asctime)s | %(levelname)s | %(message)s',
			datefmt='%Y-%d-%d %H:%M:%S',
			log_colors={
				'DEBUG': 'cyan',
				'INFO': 'white',
				'SUCCESS:': 'green',
				'WARNING': 'yellow',
				'ERROR': 'red',
				'CRITICAL': 'red,bg_white'},))

	def emit(self, record):
		msg = self.format(record)
		tqdm.tqdm.write(msg, file=sys.stderr)

ch = TqdmHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)

from blocklist import parse_master, get_domains_from_blocklist
	
def print_zones_for_domains(domains, compress=False):
	for domain in domains:
		print(ZONE_DEF  % (domain, EMPTY_ZONE), end='\n' if compress is False else ' ')

		
if __name__ == '__main__':
	blocklists = parse_master()
	
	compress = '-compress' in sys.argv
	
	hosts = set()

	for blocklist in tqdm.tqdm(blocklists):
		domains = []
		try:
			domains = get_domains_from_blocklist(blocklist)
		except Exception as ex:
			logger.error('Unable to fetch blocklist: %s because %s' % (blocklist, str(ex)))
			continue
		
		lastn = len(hosts)
		for d in domains:
			d = d.lower()
			if not d in  hosts:
				hosts.add(d)
		
		#print(domains)
		logger.info('Got %d hostnames from %s' % (len(hosts) - lastn, blocklist[-35:]))
	
	print_zones_for_domains(hosts, compress=compress)
		
