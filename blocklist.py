import re
from requests import get as http_get

# See: https://v.firebog.net/hosts/lists.php
MASTER_LIST = r'https://v.firebog.net/hosts/lists.php?type=tick'

def parse_master():
	r = http_get(MASTER_LIST)
	r.raise_for_status()
	body = r.text
	rows = list(filter(None, body.split('\n')))
	masters = []
	for row in rows:
		row = row.strip() # naked teheehhee
		if 'pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts;' in row:
			# stupid fix -.-
			masters.append('https://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml&showintro=0')
			continue
		masters.append(row)
	return masters
	
	
re_hostclean = re.compile(r'(?:[0-9]+(?:\.[0-9]+){3}|::1?|0)\s+(.+)')

def extract_hostname(line):
	m = re_hostclean.match(line)
	if m is not None:
		g = m.group(1)
		if g is not None:
			g = g.strip()
			
			if '#' in g: g = g[:g.index(' #')].strip()
			if '#' in g: raise Exception(g)
			# @todo fixme
			if g in set(('localhost', 'localhost.localdomain')): return None
		
			return g.strip()
	
	
	if '#' in line:
		line = line[:line.index(' #')].strip()
	
	if '#' in line: raise Exception(line)
	return line
	
	
def get_domains_from_blocklist(blocklist):
	r = http_get(blocklist, timeout=5)
	r.raise_for_status()
	body = r.text
	
	lines = list(filter(None, body.split('\n')))
	
	hostnames = []
	
	for line in lines:
		line = line.strip()
		if len(line) < 4: continue
		if line[0] == '#': continue
		
		# This should be done nicer because it also matches any localhost substring
		#if 'localhost' in line: continue
		
		hostname = extract_hostname(line)
		if hostname is None: continue
		if '#' in hostname: raise Exception(hostname)
		hostnames.append(hostname)
	
	return hostnames

